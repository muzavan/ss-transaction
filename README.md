# SALE STOCK TRANSACTION  #

This repository contains code for technical assessment for Sale Stock job application

### SETUP ###
**Demo for this repository can be accessed via [this-link](http://178.63.117.175:8000)**
To run this repository locally (assumed you have composer installed on your computer and php already included on your machine SYSTEM PATH variables):

1. Run `composer install` on terminal/cmd on project directory

2. Configure your database enviroment by modify .env.example and save it as .env (database supported for this project mysql only)

3. Run `php artisan migrate` to create database for project

4. Run `./vendor/bin/phpunit` to run unit test to make sure nothing's wrong

5. Run `php artisan serve` to setup temporary server to access this project locally


### Requirement Assumptions ###
These following list explains things assumed when developing this web service.

- Based on technical requirement, the flow of product and coupon is described like this.

    - When user order products, the quantity of that product is not changed until the order's status is `paid` (admin change based on proof received from user).

    - But, when user create order with a coupon, the quantity of coupon immediately changed after order created.

    - The update of product's quantity will affect not-yet-paid order. If one of the products in order is not available anymore (e.g. other order already paid and make that product's quantity can't satisfy what user want), the order will be marked as `canceled` regardless the availability of other product. The coupon will not be back to available if the order is canceled.

    - Deletion of a coupon will set coupon used in order as null, while deletion of user will also delete the order. Deletion of order, will delete all products ordered in that order. This deletion won't affect product's quantity.

- Coupon's value (either as exact value or percentage) is integer.

- Payment proof can be any files (not limited to image only)

- Fields value for email and phone number is not validated, so user input is assumed in correct format (In my humble opinion, email and phone number are not directly related to requirement specified).

- In requirement #13, it is mentioned that Admin ship order via logistic partner. Therefor, requirement #16 should be contacting logistic partner to get information about shipment progress. The logistic API is not implemented.

- Any implicit feature that commonly exists in this kind of applications is ignored, such as : Coupon update, User forgot password, etc. 

### Main Design ###
The most intriguing part of this task is about how to maintain the data consistent, especially between coupon-order-product. To handle this, database trigger is used. Database trigger is used to move validation into data layer, so any update using direct query also maintain the constraint. Moreover, in my experience, database trigger can handle same operation faster than application.
However, not all constraint is implemented on database trigger. The product update after order has been paid is handled in application layer. This is done because when it's implemented on database trigger, will cause MySQL Exception 1442 because of recursive update.

Currently, in database trigger, update on product will affect orders. Hence, if trigger also implemented in order, will cause process like this.

`Order updated => product updated => order updated => ...`

Trigger details can be looked at `/databases/migration/triggers` 

### Api List ###
Note: auth_token is generated when login. It will expires after 10 minutes after login, so need to re-login.
- `api/user/login`
    - POST  : (required parameter) email, password

- `api/user/register`
    - POST  : name,email, password

- `api/coupon/create`
    - POST  : name, is_percentage(boolean, admin only), nominal(integer), quantity(integer), first_date (format: Y-m-d), last_date (format:Y-m-d)

- `api/coupon/list`
    - GET : page(integer,optional) take(integer,optional)

- `api/product/create`
    - POST : auth_token (get from login, admin only), name, quantity(integer), description

- `api/product/list`
    - GET : page(integer,optional)

- `api/order/create`
    - POST : auth_token (get from login), name, email, phone, address, coupon_name (get from coupon list)

- `api/order/product`
    - POST : auth_token (get from login), order_id (get from order/create), product_id (get from product/list), quantity (integer)

- `api/order/finalize`
    - POST : auth_token (get from login), order_id (get from order/create)

- `api/order/payment`
    - POST : auth_token (get from login), order_id (get from order/create), proof (file)

- `api/order/paid`
    - POST : auth_token (get from login, admin only), order_id (get from order/create)

- `api/order/ship`
    - POST : auth_token (get from login, admin only), order_id (get from order/create), shipping_id

- `api/order/status`
    - POST : auth_token (get from login)

(For testing only)

- `api/dev/seed`
    - POST : (nothing, this is just used to re-seed the database for testing only, seed can be seen in `/database/seeds` folder)


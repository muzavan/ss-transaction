<?php

use Illuminate\Http\Request;
use App\User;
use App\Product;
use App\Coupon;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('user')->group(function(){
	Route::post('login','UserController@login');
	Route::post('register','UserController@register');
});

Route::prefix('coupon')->group(function(){
	Route::post('create','CouponController@create');
	Route::get('list','CouponController@list');
});

Route::prefix('product')->group(function(){
	Route::post('create','ProductController@create');
	Route::get('list','ProductController@list');
});

Route::prefix('order')->group(function(){
	Route::post('create','OrderController@create');
	Route::post('product','OrderController@addProduct');
	Route::post('finalize','OrderController@finalize');
	Route::post('payment','OrderController@uploadPayment');
	Route::post('paid','OrderController@setOrderToPaid');
	Route::post('ship','OrderController@setOrderToShipment');
	Route::post('status','OrderController@ordersStatus');
});

Route::prefix('dev')->group(function(){
	Route::post('seed',function(){

		foreach(User::all() as $user){
			$user->delete();
		}
		foreach(Product::all() as $product){
			$product->delete();
		}
		foreach(Coupon::all() as $coupon){
			$coupon->delete();
		}

		$seed = new \DatabaseSeeder;
		$seed->run();

		return response()->json(["code" => "200", "status" => "Success"]);
	});
});

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllTriggers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Create triggers
        $tables = ["coupons","orders","order_products","products"];

        foreach ($tables as $table) {                
            $trigger = file_get_contents(__DIR__."/triggers/$table.sql");
            DB::unprepared($trigger);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Triggers will be deleted in table deletion
    }
}

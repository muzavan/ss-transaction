--
-- Triggers `orders`
--

CREATE TRIGGER `Order_Coupon_Validity_Check` BEFORE INSERT ON `orders` FOR EACH ROW BEGIN 
	DECLARE msg varchar(300);
    DECLARE done INTEGER DEFAULT 0;
	DECLARE start_date date;
    DECLARE end_date date;
    DECLARE quantityx INT;
    DECLARE cur CURSOR FOR SELECT `first_date`,`last_date`,`quantity` FROM `coupons` WHERE `id` = new.coupon_id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    OPEN cur;
        ins_loop: LOOP
			FETCH cur INTO start_date,end_date,quantityx;
            LEAVE ins_loop;
		END LOOP;
    CLOSE cur;
    
    IF (quantityx <= 0) THEN
        SET msg = "Coupon is used up";
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
    END IF;

    IF(CURRENT_DATE < start_date || CURRENT_DATE > end_date) THEN
	    SET msg = "Coupon is expired";
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;        
	END IF;
    
END;

CREATE TRIGGER `Order_Coupon_Quantity_Update` AFTER INSERT ON `orders` FOR EACH ROW BEGIN 
    DECLARE msg varchar(300);    
    UPDATE `coupons` SET `quantity` = `quantity` - 1 WHERE `coupons`.`id` = new.coupon_id;
END;


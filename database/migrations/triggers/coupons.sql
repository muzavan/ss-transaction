--
-- Triggers `coupons`
--
CREATE TRIGGER `Coupon_Nominal_Valid_Check` BEFORE INSERT ON `coupons` FOR EACH ROW BEGIN
	DECLARE msg varchar(300);
	IF(new.nominal < 0 || (new.is_percentage && new.nominal > 100) ) THEN
		SET msg = concat("Not valid coupon. Please note that if the coupon is a percentage coupon, the max nominal is 100.");
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
    ELSE
    	IF(DATEDIFF(new.first_date,new.last_date) > 0) THEN
    		SET msg = concat("Not valid coupon. Last Date can't be earlier than First Date");
        	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
        ELSE
            IF(new.quantity < 0) THEN
                SET msg = concat("Not valid coupon. Quantity must be >= 0");
                SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
            END IF;
        END IF;
	END IF;
END;

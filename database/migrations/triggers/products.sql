--
-- Triggers `products`
--
CREATE TRIGGER `PRODUCT_UPDATE_ORDER` AFTER UPDATE ON `products` FOR EACH ROW BEGIN
	DECLARE done INTEGER DEFAULT 0;
    DECLARE msg varchar(300);
    DECLARE order_idx VARCHAR(36);
    DECLARE iter_count INT;
    DECLARE cur CURSOR FOR SELECT `id` FROM `orders` WHERE `orders`.id IN (SELECT DISTINCT `order_products`.`order_id` FROM `order_products` WHERE `order_products`.`product_id` = new.id AND `order_products`.`quantity` > new.quantity);
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    SET iter_count = (SELECT COUNT(1) FROM `orders` WHERE `orders`.id IN (SELECT DISTINCT `order_products`.`order_id` FROM `order_products` WHERE `order_products`.`product_id` = new.id AND `order_products`.`quantity` > new.quantity) );
    OPEN cur;
        ins_loop: LOOP
        	FETCH cur into order_idx;            
            UPDATE `orders` SET `status` = "canceled" WHERE `orders`.id = order_idx and (`status` = "submitted" or `status` = "created");
            SET iter_count = iter_count-1;

            IF (iter_count <= 0) THEN
                LEAVE ins_loop;
            END IF;
        END LOOP;
    CLOSE cur;
    

END;

CREATE TRIGGER `Product_Quantity_Check` BEFORE INSERT ON `products` FOR EACH ROW BEGIN
	DECLARE msg varchar(300);
    IF(new.quantity < 0) THEN
    	SET msg = 'Product quantity must be >= 0';
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
    END IF;
END;
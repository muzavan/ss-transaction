--
-- Triggers `order_products`
--
CREATE TRIGGER `OrderProduct_Check_Product_Availability` BEFORE INSERT ON `order_products` FOR EACH ROW BEGIN
	DECLARE msg varchar(300);
    DECLARE quantityx INT;
    DECLARE done INTEGER DEFAULT 0;
    
    DECLARE cur CURSOR FOR SELECT `quantity` FROM `products` WHERE `products`.id = new.product_id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    
    OPEN cur;
        ins_loop : LOOP
        	FETCH cur INTO quantityx;
            LEAVE ins_loop;
        END LOOP;
    CLOSE cur;
    
    IF (new.quantity <= 0) THEN
	    SET msg = "Quantity of product ordered must be > 0";
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
    ELSE 
    	IF(new.quantity > quantityx) THEN
    		SET msg = "Quantity of product can fulfill your requested quantity.";
        	SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
        END IF;
    END IF;
END;

CREATE TRIGGER `OrderProduct_Check_Product_Availability_UPDATE` BEFORE UPDATE ON `order_products` FOR EACH ROW BEGIN
    DECLARE msg varchar(300);
    DECLARE quantityx INT;
    DECLARE done INTEGER DEFAULT 0;
    
    DECLARE cur CURSOR FOR SELECT `quantity` FROM `products` WHERE `products`.id = new.product_id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
    
    OPEN cur;
        ins_loop : LOOP
            FETCH cur INTO quantityx;
            LEAVE ins_loop;
        END LOOP;
    CLOSE cur;
    
    IF (new.quantity <= 0) THEN
        SET msg = "Quantity of product ordered must be > 0";
        SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
    ELSE 
        IF(new.quantity > quantityx) THEN
            SET msg = "Quantity of product can fulfill your requested quantity.";
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = msg;
        END IF;
    END IF;
END;
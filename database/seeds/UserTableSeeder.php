<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Admin account
        User::create([
        	"name" => "admin",
        	"email" => "admin@admin.com",
        	"is_admin" => true,
        	"password" => Hash::make("admin")
        ]);

        // Customer account
        User::create([
        	"name" => "customer",
        	"email" => "customer@customer.com",
        	"is_admin" => false,
        	"password" => Hash::make("customer")
        ]);
    }
}

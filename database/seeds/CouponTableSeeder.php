<?php

use Illuminate\Database\Seeder;
use App\Coupon;

class CouponTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$first_date = new DateTime();
    	$last_date = new DateTime();
    	$last_date->modify("+ 2 days");
        Coupon::create([
        	"name" => "COUPON",
        	"quantity" => 3,
        	"is_percentage" => false,
        	"nominal" => 5000,
        	"first_date" => $first_date,
        	"last_date" => $last_date
        ]);

        Coupon::create([
        	"name" => "COUPON2",
        	"quantity" => 5,
        	"is_percentage" => true,
        	"nominal" => 50,
        	"first_date" => $first_date,
        	"last_date" => $first_date
        ]);
    }
}

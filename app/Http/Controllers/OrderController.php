<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderProduct;
use App\Coupon;
use App\User;
use App\Product;
use App\JsonResponse;

class OrderController extends Controller
{
    protected $className = "Order";
    protected $fillable = ["user_id","coupon_id","name","phone","address","email","proof","proof_type","status","shipping_id"];

    public function create(Request $request){
    	$auth_token = $request->input("auth_token");
    	
    	if($auth_token == null || User::auth($auth_token) == null){
    		return $this->notAuthorized();
    	}

    	$user = User::auth($auth_token);
    	$createData = $this->extractData($request,$this->fillable);
    	
    	if($request->has("coupon_name")){
    		$coupon = Coupon::where("name",$request->input("coupon_name"))->first();
    		if($coupon != null){
    			$createData["coupon_id"] = $coupon->id;
    		}
    	}

    	try{
    		$createData["user_id"] = $user->id;
    		$createData["status"] = "created";
    		$order = Order::create($createData);
    	}
    	catch(\PDOException $ex){
    		return $this->failCreation();
    	}

    	$json = new JsonResponse();
    	$json->setData($order->toDict());

    	return $this->toJson($json);
    }

    public function addProduct(Request $request){
    	$auth_token = $request->input("auth_token");
    	
    	if($auth_token == null || User::auth($auth_token) == null){
    		return $this->notAuthorized();
    	}

    	$user = User::auth($auth_token);
    	$data = $this->extractData($request,["order_id","product_id","quantity"]);
    	$order = Order::find($data["order_id"]);

    	if($order == null){
    		return $this->failUpdate();
    	}
    	if($order->user_id != $user->id){
    		return $this->notAuthorized();
    	}
    	try{    		
    		$order->addProduct($data["product_id"],$data["quantity"]);	
    	}
    	catch(\PDOException $ex){
    		$json = new JsonResponse(400,"Product's quantity requested is not available");
	    	return $this->toJson($json);    		
    	}
    	$order->save();

    	$json = new JsonResponse();
    	$json->setData($order->toDict());
    	$json->setData(["products" => $order->listProducts()]);

    	return $this->toJson($json);
    }

    public function finalize(Request $request){
    	$auth_token = $request->input("auth_token");

    	$user = User::auth($auth_token);
    	if($auth_token == null ||  $user == null){
    		return $this->notAuthorized();
    	}

    	$data = $this->extractData($request,["order_id"]);
    	$order = Order::find($data["order_id"]);

    	if($order == null){
    		return $this->failUpdate();
    	}
    	if($order->user_id != $user->id){
    		return $this->notAuthorized();
    	}

    	$order->finalized();
    	$order->save();

    	$json = new JsonResponse();
    	$json->setData($order->toDict());
    	$json->setData(["products" => $order->listProducts()]);
    	return $this->toJson($json);
    }

    public function uploadPayment(Request $request){
    	$auth_token = $request->input("auth_token");
    	$data = $this->extractData($request,["order_id"]);
    	$order = Order::find($data["order_id"]);
    	$user = User::auth($auth_token);

    	if($auth_token == null ||  $user == null || $user->id != $order->user_id){
    		return $this->notAuthorized();
    	}

    	if($request->hasFile("proof") && $request->file("proof")->isValid()){
    		$order->proof = base64_encode($request->proof);
    		$order->proof_type = $request->proof->extension();
    	}
    	$order->save();

    	$json = new JsonResponse();
    	$json->setData($order->toDict());
    	$json->setData(["products" => $order->listProducts()]);
    	return $this->toJson($json);
    }

    public function setOrderToPaid(Request $request){
    	if(!$this->isAuthorize($request)){
    		return $this->notAuthorized();
    	}

    	$data = $this->extractData($request,["order_id"]);
    	$order = Order::find($data["order_id"]);

    	if($order->proof == null){
    		$json = new JsonResponse(400, "Can set to paid if payment haven't uploaded");
    		return $this->toJson($json);
    	}

    	$order->paid();
    	$order->save();

    	$json = new JsonResponse();
    	$json->setData($order->toDict());
    	
    	return $this->toJson($json);
    }

    public function setOrderToShipment(Request $request){
    	if(!$this->isAuthorize($request)){
    		return $this->notAuthorized();
    	}

    	$data = $this->extractData($request,["order_id","shipping_id"]);
    	$order = Order::find($data["order_id"]);

    	if($order->status != "paid"){
    		$json = new JsonResponse(400, "Can set shipment if order status is not paid");
    		return $this->toJson($json);
    	}
    	$order->shipping_id = $data["shipping_id"];
    	$order->save();

    	$json = new JsonResponse();
    	$json->setData($order->toDict());
    	
    	return $this->toJson($json);
    }

    public function ordersStatus(Request $request){
    	$auth_token = $request->input("auth_token");

    	if($auth_token == null || User::auth($auth_token) == null){
    		return $this->notAuthorized();
    	}
    	$user = User::auth($auth_token);

    	$orders = Order::where('user_id',$user->id)->get();
    	$data = [];

    	foreach($orders as $order){
    		$tdata = $order->toDict();
    		$tdata["products"] = $order->listProducts();
    		array_push($data, $tdata);
    	}
    	$json = new JsonResponse();
    	$json->setData($data);
    	return $this->toJson($json);
    }
}

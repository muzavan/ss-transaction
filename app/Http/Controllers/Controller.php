<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\JsonResponse;
use App\User;
use Hash;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $integerFields = ["quantity"];
    private $hashedFields = ["password"];
    private $booleanFields = ["is_percentage"];
    private $dateFields = ["first_date","last_date"];
    
    protected $className = "";
    protected $fillable = [];

    protected function toJson(JsonResponse $json){
    	return response()->json($json->toJson());
    }

    protected function extractData(Request $request, $keys){
    	$ret = [];

    	foreach($keys as $key){
    		if(in_array($key,$this->integerFields)){
    			$ret[$key] = intval($request->input($key));
    		}
            else if(in_array($key,$this->hashedFields)){
                $ret[$key] = Hash::make($request->input($key));
            }
            else if(in_array($key, $this->booleanFields)){
                $ret[$key] = $request->input($key) != null && (strtolower($request->input($key)) == "true");
            }
            else if(in_array($key, $this->dateFields)){
                $ret[$key] = date("Y-m-d",strtotime($request->input($key)));
            }
    		else{
    			$ret[$key] = $request->input($key);	
    		}
    		
    	}
    	return $ret;    	
    }

    protected function isAuthorize(Request $request){
    	$auth_token = $request->input("auth_token");
    	$user = User::auth($auth_token);
    	return ($user != null && $user->is_admin);
    }

    protected function notAuthorized(){
    	$json = new JsonResponse(502,"Not Authorized.");

    	return $this->toJson($json);
    }

    protected function failCreation(){
    	$json = new JsonResponse(400,$this->className." creation is failed. Please refer to the specification.");

    	return $this->toJson($json);
    }

    protected function failUpdate(){
        $json = new JsonResponse(400,$this->className." update is failed. Please refer to the specification.");

        return $this->toJson($json);
    }
}

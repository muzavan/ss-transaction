<?php

namespace App\Http\Controllers;

use App\Coupon;
use App\JsonResponse;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    protected $className = "Coupon";
    protected $fillable = ["name","quantity","is_percentage","nominal","first_date","last_date"];

    public function create(Request $request){
    	if(!$this->isAuthorize($request)){
    		return $this->notAuthorized();
    	}

    	$coupon = null;
    	try{
    		$coupon = Coupon::create($this->extractData($request,$this->fillable));	
    	}
    	catch(\PDOException $ex){
    		return $this->failCreation();
    	}
    	
        $json = new JsonResponse();
        $json->setData($coupon->toDict());
    	return $this->toJson($json);
    }

    public function list(Request $request){
        $page = $request->input("page") != null ? intval($request->input("page")) : 1;
        $take = $request->input("take") != null ? intval($request->input("take")) : 5;
        $offset = ($page-1) * $take;

        $coupons = Coupon::orderBy("name")->skip($offset)->take($take)->get();
        $data = [];
        
        foreach($coupons as $coupon){
            array_push($data, $coupon->toDict());
        }

        $json = new JsonResponse();
        $json->setData($data);

        return $this->toJson($json); 
    }
}

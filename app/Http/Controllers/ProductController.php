<?php

namespace App\Http\Controllers;

use App\Product;
use App\JsonResponse;
use Illuminate\Http\Request;

class ProductController extends Controller
{
	protected $className = "Product";
    protected $fillable = ["name","quantity","description"];

    public function create(Request $request){
    	if(!$this->isAuthorize($request)){
    		return $this->notAuthorized();
    	}

    	$product = null;
    	try{
    		$product = Product::create($this->extractData($request,$this->fillable));	
    	}
    	catch(\PDOException $ex){
    		return $this->failCreation();
    	}
    	
        $json = new JsonResponse();
        $json->setData($product->toDict());
    	return $this->toJson($json);
    }

    public function list(Request $request){
        $page = $request->input("page") != null ? intval($request->input("page")) : 1;
        $take = $request->input("take") != null ? intval($request->input("take")) : 5;
        $offset = ($page-1) * $take;

        $products = Product::orderBy("name")->skip($offset)->take($take)->get();
        $data = [];
        
        foreach($products as $product){
            array_push($data, $product->toDict());
        }

        $json = new JsonResponse();
        $json->setData($data);

        return $this->toJson($json); 
    }
}

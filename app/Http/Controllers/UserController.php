<?php

namespace App\Http\Controllers;

use App\User;
use App\JsonResponse;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $className = "User";

    public function login(Request $request){
    	$email = $request->input('email');
    	$password = $request->input('password');

    	$auth = User::login($email,$password);
    	
    	$json = null;
    	if($auth == null){
    		$json = new JsonResponse(400,"Error. Please check your credentials account");
    	}
    	else{
    		$json = new JsonResponse();
    		$json->setData(["token" => $auth]);
    	}
    	return $this->toJson($json);
    }

    public function register(Request $request){
    	$json = null;
    	try{
    		$user = User::create($this->extractData($request,["name","email","password"])
    		);
    		$json = new JsonResponse();
            $json->setData($user->toArray());
    	}
    	catch(\PDOException $ex){
    		return $this->failCreation();
    	}
    	return $this->toJson($json);
    }
}

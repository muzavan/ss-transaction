<?php

namespace App;

use App\BaseModel as Model;

class Product extends Model
{
    protected $fillable = ["name","quantity","description"];
}

<?php

namespace App;
use Webpatser\Uuid\Uuid;

trait Uuids
{
	protected static function boot(){
		parent::boot();

		// Generate UUID when model is being created
		static::creating(function($model){
			$model->{$model->getKeyName()} = Uuid::generate()->string;
		});
	}
}
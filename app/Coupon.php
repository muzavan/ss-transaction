<?php

namespace App;

use App\BaseModel as Model;

class Coupon extends Model
{
    protected $fillable = ["name","first_date","last_date","is_percentage","nominal","quantity"];
}

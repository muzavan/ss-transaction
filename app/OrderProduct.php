<?php

namespace App;

use App\BaseModel as Model;

class OrderProduct extends Model
{
    protected $fillable = ["id","order_id","product_id","quantity"];
}

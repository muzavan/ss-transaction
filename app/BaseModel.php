<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model{
	use Uuids;
	/**
     * Removing auto-increment IDs
     *
    **/
    public $incrementing = false;
    /**
     * Remove timestamp
     *
    **/
    public $timestamps = false;

    public function toDict(){
    	return $this->toArray();
    }
}
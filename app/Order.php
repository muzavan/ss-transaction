<?php

namespace App;

use App\Order;
use App\OrderProduct;
use App\BaseModel as Model;

class Order extends Model
{
    public $fillable = ["user_id","coupon_id","name","phone","address","email","proof","proof_type","status","shipping_id"];    
    

    public function finalized(){
        $this->status = "submitted";
        $this->save();
    }
    
    public function paid(){
        // Handle the status change on code because in trigger will throw MySQL 1442
        $this->status = "paid";
        $this->save();

        // Update product's quantity
        $orderProducts = OrderProduct::where("order_id",$this->id)->get();

        foreach ($orderProducts as $orderProduct) {
            $product = Product::find($orderProduct->product_id);

            if($product == null){
                continue;
            }

            $quantity = $product->quantity - $orderProduct->quantity;


            $product->quantity = $quantity;
            $product->save(); 
        }
    }

    public function addProduct($product_id, $quantity){
        $orderProduct = OrderProduct::where('order_id',$this->id)->where('product_id',$product_id)->first();
        if($orderProduct == null){
            //CreateNew
            OrderProduct::create([
                "order_id" => $this->id,
                "product_id" => $product_id,
                "quantity" => $quantity
            ]);
        }
        else{
            $productQuantity = Product::find($product_id)->quantity;
            $newQuantity = $orderProduct->quantity + $quantity;
            if($productQuantity < $newQuantity){
                throw new \PDOException();
            }
            $orderProduct->quantity = $newQuantity;
            $orderProduct->save(); 
        }
    }

    public function listProducts(){
        $products = OrderProduct::where('order_id',$this->id)->get();
        $data = [];
        foreach($products as $product){
            $data[$product->product_id] = $product->quantity; 
        }

        return $data;
    }
}

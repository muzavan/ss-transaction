<?php

namespace App;

class JsonResponse{
	private $code = 200;
	private $message = "Successful";
	private $data = [];

	function __construct($code=null,$message=null){
		if(isset($code) && isset($message)){		
			$this->code = $code;
			$this->message = $message;
		}
	}

	function setData($arrayData){
		foreach($arrayData as $key => $value){
			$this->data[$key] = $value;
		}
	}

	function toJson(){
		$ret = [];
		$ret["code"] = $this->code;
		$ret["message"] = $this->message;
		$ret["data"] = $this->data;

		return $ret; 
	}
}
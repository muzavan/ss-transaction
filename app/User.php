<?php

namespace App;

use DateTime;
use Hash;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;
    use Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'is_admin','token','valid_until'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password','token'
    ];

    /**
     * Removing auto-increment IDs
     *
    **/
    public $incrementing = false;

    /**
     * Remove timestamp
     *
    **/
    public $timestamps = false;

    /**
     * Login, given credentials (email and password), will return token used for transaction
     * @return string
    **/
    public static function login($email, $password){
        $user = User::where('email',$email)->first();
        if($user == null){
            return null;   
        }


        if(Hash::check($password,$user->password)){
            // Generate authentication token
            $cur_time = new DateTime();
            $user->token = md5($cur_time->format('Y-m-d H:i:s').$email.$user->name);

            $cur_time->modify("+ 10 minutes");
            $user->valid_until = $cur_time;

            $user->save();

            return $user->token;
        }

        return null;
    }

    /**
     * Authentication, given token, will return user for transaction (should be used on server only!)
     * @return User
    **/
    public static function auth($token){
        if($token == null || $token == ""){
            return null;
        }
        
        $user = User::where('token',$token)->first();
        if($user == null){            
            return null;
        }

        $cur_time = new DateTime();
        $valid_until = new DateTime($user->valid_until);
        if($cur_time > $valid_until){
            $user->token = null;
            $user->valid_until = null;

            return null;
        }

        return $user;
    }    
}

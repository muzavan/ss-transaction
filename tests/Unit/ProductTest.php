<?php

namespace Tests\Unit;

use App\Product;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductTest extends TestCase
{
    public function testInvalidQuantity(){
    	$er = null;
    	try{
    		Product::create([
    			"name" => "Meja",
    			"quantity" => -1,
    			"description" => "Berkualitas"
    		]);
    	}
    	catch(\PDOException $ex){
    		$er = $ex;
    	}

    	$this->assertTrue($er != null);
    }
}

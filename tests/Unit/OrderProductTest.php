<?php

namespace Tests\Unit;

use App\Order;
use App\OrderProduct;
use App\Product;
use App\User;

use Hash;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrderProductTest extends TestCase
{
	protected function setUp(){
		parent::setUp();

		// Create dummy user, coupon, order, and product		
		User::create([
			"name" => "dummy",
			"email" => "dummy@dummy.com",
			"password" => Hash::make("dummy")
		]);

		Product::create([
			"name" => "Meja Dummy",
			"quantity" => 5,
			"description" => "Berkualitas"
		]);

		$user = User::where("email","dummy@dummy.com")->first();

 		Order::create([
 			"user_id" => $user->id,
 			"name" => "Dummy Name",
 			"phone" => "00000000",
 			"address" => "This is an address",
 			"email" => "email@email.com"
 		]);
	}

	protected function tearDown(){
		User::where("email","dummy@dummy.com")->delete(); //automatically delete order
		Product::where("name","Meja Dummy")->delete();
		parent::tearDown();
	}

   	public function testInvalidProductQuantity(){
   		$order = Order::where("name","Dummy Name")->first();
   		$product = Product::where("name","Meja Dummy")->first();

   		$er = null;
   		try{
   			OrderProduct::create([
   				"order_id" => $order->id,
   				"product_id" => $product->id,
   				"quantity" => 0
   			]);
   		}
   		catch(\PDOException $ex){
   			$er = $ex;
   		}

   		$this->assertTrue($er != null);
   	}

   	public function testUpdateProductQuantityBasedOnOrder(){
   		// Product's quantity will be reduced if the current order is marked as 'paid' (admin already verified the payment)

   		// Add new order of meja
   		$order = Order::where("name","Dummy Name")->first();
   		$product = Product::where("name","Meja Dummy")->first();

   		OrderProduct::create([
			"order_id" => $order->id,
			"product_id" => $product->id,
			"quantity" => 2
		]);
		
		$order->paid();
   		$order->save();

   		$product = $product->fresh();
   		$this->assertTrue($product->quantity == 3); // 5 'Meja Dummy's, then 2 of them are bought
   	}

   	public function testUpdateOrderBasedOnProduct(){
   		// Add new order of meja
   		$order = Order::where("name","Dummy Name")->first();
   		$product = Product::where("name","Meja Dummy")->first();

   		OrderProduct::create([
			"order_id" => $order->id,
			"product_id" => $product->id,
			"quantity" => 2
		]);

   		$product->quantity = 1;
   		$product->save();

   		$order = $order->fresh();
   		$this->assertTrue($order->status == "canceled");
   	}
}

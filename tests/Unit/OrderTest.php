<?php

namespace Tests\Unit;

use App\Coupon;
use App\Order;
use App\User;
use DateTime;
use Hash;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrderTest extends TestCase
{
	protected function setUp(){
		parent::setUp();

		// Create dummy user and dummy coupon
		User::create([
			"name" => "dummy",
			"email" => "dummy@dummy.com",
			"password" => Hash::make("dummy")
		]);

		Coupon::create([
			"name" => "coupon",
			"is_percentage" => false,
			"nominal" => 500,
			"first_date" => new DateTime(),
			"last_date" => new DateTime(),
			"quantity" => 1
		]);
	}

	protected function tearDown(){
		User::where("email","dummy@dummy.com")->delete();
		Coupon::where("name","coupon")->delete();
		parent::tearDown();
	}

 	public function testOrderWithoutCoupon(){
 		$user = User::where("email","dummy@dummy.com")->first();

 		$order = Order::create([
 			"user_id" => $user->id,
 			"name" => "Name",
 			"phone" => "00000000",
 			"address" => "This is an address",
 			"email" => "email@email.com"
 		]);

 		$this->assertTrue($order->id != null);

 		// Cleanup
 		Order::find($order->id)->delete();
 	}

 	public function testOrderWithInvalidCouponEmpty(){
 		$user = User::where("email","dummy@dummy.com")->first();

 		$coupon = Coupon::where("name","coupon")->first();

 		$order = Order::create([
 			"user_id" => $user->id,
 			"coupon_id" => $coupon->id,
 			"name" => "Name",
 			"phone" => "00000000",
 			"address" => "This is an address",
 			"email" => "email@email.com"
 		]);

 		$this->assertTrue($order->id != null); // Expected to be success, and make coupon quantity 0

 		$er = null;
 		try{
 			$order = Order::create([
	 			"user_id" => $user->id,
	 			"coupon_id" => $coupon->id,
	 			"name" => "New Name",
	 			"phone" => "00000000",
	 			"address" => "This is an address",
	 			"email" => "email@email.com"
 			]);
 		}
 		catch(\PDOException $ex){
 			// Expected to fail, because coupon already used up
 			$er = $ex;
 		}

 		$this->assertTrue($er != null);
 	}

 	public function testOrderWithInvalidCouponDate(){
 		$user = User::where("email","dummy@dummy.com")->first();

 		$coupon = Coupon::where("name","coupon")->first();

 		$first_date = new DateTime();
 		$last_date = new DateTime();

 		$first_date->modify("- 1 days");
 		$last_date->modify("- 1 days");

 		$coupon->first_date = $first_date;
 		$coupon->last_date = $last_date;
 		$coupon->save();

 		$er = null;
 		try{
 			$order = Order::create([
	 			"user_id" => $user->id,
	 			"coupon_id" => $coupon->id,
	 			"name" => "New Name",
	 			"phone" => "00000000",
	 			"address" => "This is an address",
	 			"email" => "email@email.com"
 			]);	
 		}
 		catch(\PDOException $ex){
 			// Expected to fail, because coupon already expired
 			$er = $ex;
 		}

 		$this->assertTrue($er != null);
 	}  
}

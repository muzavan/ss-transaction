<?php

namespace Tests\Unit;

use App\User;
use Hash;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
	protected function setUp(){
		parent::setUp();
		User::create([
			"name" => "dummy",
			"email" => "dummy@dummy.com",
			"password" => Hash::make("dummy")
		]);
	}

	protected function tearDown(){
		$deleted = User::where('email','dummy@dummy.com')->delete();
		parent::tearDown();
	}

	public function testFailedLogin(){
		$token = User::login("dummy@dummy.com","dum");

		$this->assertNull($token);

		$token = User::login("dummy@gmail.com","dummy");

		$this->assertNull($token);
	}

	public function testSuccessLogin(){
		$token = User::login("dummy@dummy.com","dummy");

		$this->assertTrue($token != null);
	}

	public function testFailedAuth(){
		$token = User::login("dummy@dummy.com","dummy");

		$user = User::auth($token."...");

		$this->assertNull($user);		
	}

	public function testSuccessAuth(){
		$token = User::login("dummy@dummy.com","dummy");

		$user = User::auth($token);

		$this->assertEquals($user->email,"dummy@dummy.com");		
	}
}

<?php

namespace Tests\Unit;

use App\Coupon;
use DateTime;
use Exception;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CouponTest extends TestCase
{
    protected function setUp(){
		parent::setUp();
		$first_date = new DateTime();
		$last_date = new DateTime();
		Coupon::create([
			"name" => "C0UP0N",
			"is_percentage" => false,
			"nominal" => 10000,
			"first_date" => $first_date,
			"last_date" => $last_date
		]);
	}

	protected function tearDown(){
		Coupon::where("name","C0UP0N")->delete();
	}

	public function testInvalidCouponSameName(){
		$first_date = new DateTime();
    	$last_date = new DateTime();
    	$last_date->modify("+ 2 days");

    	$er = null;
    	try{
	    	// Invalid because the same name
	    	$coupon = Coupon::create([
	    		"name" => "C0UP0N",
	        	"is_percentage" => false,
	        	"nominal" => 10000,
	        	"first_date" => $first_date,
	        	"last_date" => $last_date
	    	]);	
    	}
    	catch(Exception $ex){
    		$er = $ex;
    	}

    	$this->assertTrue($er != null);
	}

	public function testInvalidCouponPercentage(){
		$first_date = new DateTime();
    	$last_date = new DateTime();
    	$last_date->modify("+ 2 days");

    	$er = null;
    	try{
	    	// Invalid because the percentage over 100
	    	$coupon = Coupon::create([
	    		"name" => "COUPON",
	        	"is_percentage" => true,
	        	"nominal" => 500,
	        	"first_date" => $first_date,
	        	"last_date" => $last_date
	    	]);
    	}
    	catch(Exception $ex){
    		$er = $ex;
    	}

    	$this->assertTrue($er != null);
	}
   
    public function testInvalidCouponDate(){
    	$first_date = new DateTime();
    	$last_date = new DateTime();
    	$last_date->modify("+ 2 days");
   		
    	$er = null;
    	try{
	    	// Invalid because the last date is earlier than the first date
	    	$coupon = Coupon::create([
	    		"name" => "COUPON",
	        	"is_percentage" => false,
	        	"nominal" => 500,
	        	"first_date" => $last_date,
	        	"last_date" => $first_date
	    	]);
    	}
    	catch(Exception $ex){
    		$er = $ex;
    	}

    	$this->assertTrue($er != null);
    	
    }

}
